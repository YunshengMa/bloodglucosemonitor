﻿namespace GlucoseGUI
{
    partial class PatientDosage
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.PatientDosageTitleLabel = new System.Windows.Forms.Label();
            this.DosageTextLabel = new System.Windows.Forms.Label();
            this.DosageAmountLabel = new System.Windows.Forms.Label();
            this.BeginDateCalender = new System.Windows.Forms.MonthCalendar();
            this.BeginDateLabel = new System.Windows.Forms.Label();
            this.EndDateLabel = new System.Windows.Forms.Label();
            this.EndDateCalender = new System.Windows.Forms.MonthCalendar();
            this.PatientIdTextBox = new System.Windows.Forms.TextBox();
            this.textBox2 = new System.Windows.Forms.TextBox();
            this.PatientNameTextBox = new System.Windows.Forms.Label();
            this.PatientGlucoseLevelTextBox = new System.Windows.Forms.Label();
            this.GetDosageButton = new System.Windows.Forms.Button();
            this.OkButton = new System.Windows.Forms.Button();
            this.SuspendLayout();
            // 
            // PatientDosageTitleLabel
            // 
            this.PatientDosageTitleLabel.AutoSize = true;
            this.PatientDosageTitleLabel.Font = new System.Drawing.Font("Microsoft Sans Serif", 11.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.PatientDosageTitleLabel.Location = new System.Drawing.Point(12, 9);
            this.PatientDosageTitleLabel.Name = "PatientDosageTitleLabel";
            this.PatientDosageTitleLabel.Size = new System.Drawing.Size(123, 18);
            this.PatientDosageTitleLabel.TabIndex = 0;
            this.PatientDosageTitleLabel.Text = "Patient Dosage";
            // 
            // DosageTextLabel
            // 
            this.DosageTextLabel.AutoSize = true;
            this.DosageTextLabel.Font = new System.Drawing.Font("Microsoft Sans Serif", 15.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.DosageTextLabel.Location = new System.Drawing.Point(445, 380);
            this.DosageTextLabel.Name = "DosageTextLabel";
            this.DosageTextLabel.Size = new System.Drawing.Size(99, 25);
            this.DosageTextLabel.TabIndex = 1;
            this.DosageTextLabel.Text = "Dosage:";
            // 
            // DosageAmountLabel
            // 
            this.DosageAmountLabel.AutoSize = true;
            this.DosageAmountLabel.Font = new System.Drawing.Font("Microsoft Sans Serif", 15.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.DosageAmountLabel.Location = new System.Drawing.Point(550, 380);
            this.DosageAmountLabel.Name = "DosageAmountLabel";
            this.DosageAmountLabel.Size = new System.Drawing.Size(25, 25);
            this.DosageAmountLabel.TabIndex = 2;
            this.DosageAmountLabel.Text = "0";
            // 
            // BeginDateCalender
            // 
            this.BeginDateCalender.Location = new System.Drawing.Point(124, 36);
            this.BeginDateCalender.MaxSelectionCount = 1;
            this.BeginDateCalender.Name = "BeginDateCalender";
            this.BeginDateCalender.TabIndex = 3;
            // 
            // BeginDateLabel
            // 
            this.BeginDateLabel.AutoSize = true;
            this.BeginDateLabel.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.BeginDateLabel.Location = new System.Drawing.Point(13, 104);
            this.BeginDateLabel.Name = "BeginDateLabel";
            this.BeginDateLabel.Size = new System.Drawing.Size(99, 20);
            this.BeginDateLabel.TabIndex = 4;
            this.BeginDateLabel.Text = "Begin Date";
            // 
            // EndDateLabel
            // 
            this.EndDateLabel.AutoSize = true;
            this.EndDateLabel.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.EndDateLabel.Location = new System.Drawing.Point(13, 301);
            this.EndDateLabel.Name = "EndDateLabel";
            this.EndDateLabel.Size = new System.Drawing.Size(85, 20);
            this.EndDateLabel.TabIndex = 5;
            this.EndDateLabel.Text = "End Date";
            // 
            // EndDateCalender
            // 
            this.EndDateCalender.Location = new System.Drawing.Point(124, 209);
            this.EndDateCalender.MaxSelectionCount = 1;
            this.EndDateCalender.Name = "EndDateCalender";
            this.EndDateCalender.TabIndex = 6;
            // 
            // PatientIdTextBox
            // 
            this.PatientIdTextBox.Location = new System.Drawing.Point(439, 36);
            this.PatientIdTextBox.Name = "PatientIdTextBox";
            this.PatientIdTextBox.Size = new System.Drawing.Size(100, 20);
            this.PatientIdTextBox.TabIndex = 7;
            // 
            // textBox2
            // 
            this.textBox2.Location = new System.Drawing.Point(439, 84);
            this.textBox2.Name = "textBox2";
            this.textBox2.Size = new System.Drawing.Size(100, 20);
            this.textBox2.TabIndex = 8;
            // 
            // PatientNameTextBox
            // 
            this.PatientNameTextBox.AutoSize = true;
            this.PatientNameTextBox.Location = new System.Drawing.Point(381, 43);
            this.PatientNameTextBox.Name = "PatientNameTextBox";
            this.PatientNameTextBox.Size = new System.Drawing.Size(52, 13);
            this.PatientNameTextBox.TabIndex = 9;
            this.PatientNameTextBox.Text = "Patient Id";
            // 
            // PatientGlucoseLevelTextBox
            // 
            this.PatientGlucoseLevelTextBox.AutoSize = true;
            this.PatientGlucoseLevelTextBox.Location = new System.Drawing.Point(358, 87);
            this.PatientGlucoseLevelTextBox.Name = "PatientGlucoseLevelTextBox";
            this.PatientGlucoseLevelTextBox.Size = new System.Drawing.Size(75, 13);
            this.PatientGlucoseLevelTextBox.TabIndex = 10;
            this.PatientGlucoseLevelTextBox.Text = "Glucose Level";
            // 
            // GetDosageButton
            // 
            this.GetDosageButton.Location = new System.Drawing.Point(439, 127);
            this.GetDosageButton.Name = "GetDosageButton";
            this.GetDosageButton.Size = new System.Drawing.Size(100, 24);
            this.GetDosageButton.TabIndex = 11;
            this.GetDosageButton.Text = "Get Dosage Amount";
            this.GetDosageButton.UseVisualStyleBackColor = true;
            this.GetDosageButton.Click += new System.EventHandler(this.GetDosageButton_Click);
            // 
            // OkButton
            // 
            this.OkButton.Location = new System.Drawing.Point(17, 406);
            this.OkButton.Name = "OkButton";
            this.OkButton.Size = new System.Drawing.Size(75, 23);
            this.OkButton.TabIndex = 12;
            this.OkButton.Text = "Ok";
            this.OkButton.UseVisualStyleBackColor = true;
            this.OkButton.Click += new System.EventHandler(this.OkButton_Click);
            // 
            // PatientDosage
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(587, 441);
            this.Controls.Add(this.OkButton);
            this.Controls.Add(this.GetDosageButton);
            this.Controls.Add(this.PatientGlucoseLevelTextBox);
            this.Controls.Add(this.PatientNameTextBox);
            this.Controls.Add(this.textBox2);
            this.Controls.Add(this.PatientIdTextBox);
            this.Controls.Add(this.EndDateCalender);
            this.Controls.Add(this.EndDateLabel);
            this.Controls.Add(this.BeginDateLabel);
            this.Controls.Add(this.BeginDateCalender);
            this.Controls.Add(this.DosageAmountLabel);
            this.Controls.Add(this.DosageTextLabel);
            this.Controls.Add(this.PatientDosageTitleLabel);
            this.Name = "PatientDosage";
            this.Text = "PatientDosage";
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Label PatientDosageTitleLabel;
        private System.Windows.Forms.Label DosageTextLabel;
        private System.Windows.Forms.Label DosageAmountLabel;
        private System.Windows.Forms.MonthCalendar BeginDateCalender;
        private System.Windows.Forms.Label BeginDateLabel;
        private System.Windows.Forms.Label EndDateLabel;
        private System.Windows.Forms.MonthCalendar EndDateCalender;
        private System.Windows.Forms.TextBox PatientIdTextBox;
        private System.Windows.Forms.TextBox textBox2;
        private System.Windows.Forms.Label PatientNameTextBox;
        private System.Windows.Forms.Label PatientGlucoseLevelTextBox;
        private System.Windows.Forms.Button GetDosageButton;
        private System.Windows.Forms.Button OkButton;
    }
}